package com.gitlab.spacetrucker.regexprocessing;

import java.util.regex.Pattern;

import org.junit.Assert;
import org.junit.Test;

public class PatternProcessingInstructionTest {

	@Test
	public void foo() {
		PatternProcessingInstruction processingInstruction = new PatternProcessingInstruction(Pattern.compile("a+"),
				"b");

		String result = processingInstruction.process("acaac");

		Assert.assertEquals("bcbc", result);
	}

	@Test(expected = IllegalArgumentException.class)
	public void shouldThrowIfPatternIllegal() {
		String illegalPattern = "(a+";

		PatternProcessingInstruction.createProcessingInstruction(illegalPattern, "a");
	}

	@Test
	public void shouldCreateInstanceIfPatternLegal() {
		PatternProcessingInstruction result = PatternProcessingInstruction.createProcessingInstruction("(a+)", "a");

		Assert.assertNotNull(result);
	}

}
