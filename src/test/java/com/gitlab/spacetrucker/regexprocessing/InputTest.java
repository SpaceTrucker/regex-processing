package com.gitlab.spacetrucker.regexprocessing;

import java.io.ByteArrayInputStream;
import java.io.IOException;
import java.io.InputStream;
import java.nio.charset.Charset;
import java.util.Arrays;

import org.junit.Assert;
import org.junit.Test;

public class InputTest {

	private final Charset charset = Charset.forName("UTF-8");

	@Test
	public void shouldReadInput() {
		String input = "abcd";
		ByteArrayInputStream inputStream = new ByteArrayInputStream(input.getBytes(charset));

		CharSequence result = Input.readInput(inputStream, charset);

		Assert.assertEquals(input, String.valueOf(result));
	}

	@Test
	public void shouldReadAllInput() {
		char[] chars = new char[Short.MAX_VALUE];
		Arrays.fill(chars, 'a');
		String largeInput = new String(chars);
		ByteArrayInputStream inputStream = new ByteArrayInputStream(largeInput.getBytes(charset));

		CharSequence result = Input.readInput(inputStream, charset);

		Assert.assertEquals(largeInput, String.valueOf(result));
	}

	@Test(expected = IllegalStateException.class)
	public void shouldThrowIllegalStateIfUnableToReadInput() {
		InputStream input = new InputStream() {

			@Override
			public int read() throws IOException {
				throw new IOException();
			}
		};

		Input.readInput(input, charset);
	}

	@Test(expected = IllegalStateException.class)
	public void shouldThrowIllegalStateIfNotEnoughMemoryToReadInput() {
		InputStream input = new InputStream() {

			@Override
			public int read() throws IOException {
				throw new OutOfMemoryError();
			}
		};

		Input.readInput(input, charset);
	}
}
