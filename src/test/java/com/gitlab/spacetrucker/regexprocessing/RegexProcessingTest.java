package com.gitlab.spacetrucker.regexprocessing;

import java.io.ByteArrayInputStream;
import java.io.ByteArrayOutputStream;
import java.io.InputStream;
import java.io.PrintStream;
import java.util.Arrays;

import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import org.mockito.ArgumentCaptor;
import org.mockito.Captor;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.MockitoAnnotations;

public class RegexProcessingTest {

	@Mock
	private ProcessingInstructionParser processingInstructionParser;

	@Mock
	private ProcessingInstruction processingInstruction;

	@Captor
	ArgumentCaptor<String> processInputCaptor;

	private RegexProcessingOptions options;
	private ByteArrayOutputStream output = new ByteArrayOutputStream();


	@Before
	public void beforeMethod() {
		MockitoAnnotations.initMocks(this);

		options = new RegexProcessingOptions(Arrays.asList("[a-z]", ""), false);

	}

	@Test
	public void foo() {
		String input = "test";
		InputStream inputStream = new ByteArrayInputStream(input.getBytes());
		PrintStream outputPrintStream = new PrintStream(output);
		RegexProcessing regexProcessing = new RegexProcessing(options, inputStream, outputPrintStream,
				processingInstructionParser);
		Mockito.when(processingInstructionParser.parseProcessingInstructions(options.getCommandlineProcessingInstructions())).thenReturn(processingInstruction);

		regexProcessing.run();
		
		Mockito.verify(processingInstruction).process(processInputCaptor.capture());
		Assert.assertEquals(input, String.valueOf(processInputCaptor.getValue()));
	}

}
