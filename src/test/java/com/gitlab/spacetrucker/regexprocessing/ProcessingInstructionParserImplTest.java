package com.gitlab.spacetrucker.regexprocessing;

import java.util.Arrays;
import java.util.Collections;

import org.junit.Assert;
import org.junit.Test;

public class ProcessingInstructionParserImplTest {

	private ProcessingInstructionParserImpl processingInstructionParserImpl = new ProcessingInstructionParserImpl();

	@Test
	public void shouldParseEmptyListToInstanceWithNoProcessingInstructions() {
		ChainedProcessingInstruction result = processingInstructionParserImpl
				.parseProcessingInstructions(Collections.emptyList());

		Assert.assertNotNull(result);
		Assert.assertTrue(result.getProcessingInstructions().isEmpty());
	}

	@Test(expected = IllegalArgumentException.class)
	public void shouldThrowIfListSizeOdd() {
		processingInstructionParserImpl
				.parseProcessingInstructions(Collections.singletonList(""));
	}

	@Test(expected = IllegalArgumentException.class)
	public void shouldThrowIfRegularExpressionInvalid() {
		String invalidRegex = "[a-z";
		processingInstructionParserImpl.parseProcessingInstructions(Arrays.asList(invalidRegex, ""));
	}

	@Test
	public void shouldCreateInstanceWithProcessingInstruction() {
		String regularExpression = "[a-z]";
		String replacement = "";

		ChainedProcessingInstruction result = processingInstructionParserImpl
				.parseProcessingInstructions(Arrays.asList(regularExpression, replacement));

		Assert.assertEquals(1, result.getProcessingInstructions().size());
		PatternProcessingInstruction element = (PatternProcessingInstruction) result.getProcessingInstructions().get(0);
		Assert.assertEquals(regularExpression, element.getPattern().pattern());
		Assert.assertEquals(replacement, element.getReplacement());
	}

	@Test
	public void shouldCreateInstanceMultipleWithMultipleProcessingInstructionsForMultipleInput() {
		ChainedProcessingInstruction result = processingInstructionParserImpl
				.parseProcessingInstructions(Arrays.asList("[a-z]", "", "\\d", ""));
		
		Assert.assertEquals(2, result.getProcessingInstructions().size());
	}
}
