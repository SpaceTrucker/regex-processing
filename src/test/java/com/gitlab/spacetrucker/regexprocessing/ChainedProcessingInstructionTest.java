package com.gitlab.spacetrucker.regexprocessing;

import java.util.Arrays;
import java.util.Collections;

import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.MockitoAnnotations;

public class ChainedProcessingInstructionTest {

	@Mock
	private ProcessingInstruction processingInstruction1;

	@Mock
	private ProcessingInstruction processingInstruction2;

	@Before
	public void beforeMethod() {
		MockitoAnnotations.initMocks(this);
	}

	@Test
	public void shouldPassPreviousResultToNextInstructionWhenProcessing() {
		ChainedProcessingInstruction processingInstruction = new ChainedProcessingInstruction(
				Arrays.asList(processingInstruction1, processingInstruction2));
		String input = "123";
		String resultProcessingInstruction1 = "abc";
		Mockito.when(processingInstruction1.process(input)).thenReturn(resultProcessingInstruction1);

		processingInstruction.process(input);

		Mockito.verify(processingInstruction2).process(resultProcessingInstruction1);
	}

	@Test
	public void shouldReturnResultOfLastProcessingInstructionWhenProcessing() {
		ChainedProcessingInstruction processingInstruction = new ChainedProcessingInstruction(
				Arrays.asList(processingInstruction1, processingInstruction2));
		String input = "123";
		String resultProcessingInstruction2 = "abc";
		Mockito.when(processingInstruction2.process(Mockito.any())).thenReturn(resultProcessingInstruction2);

		CharSequence result = processingInstruction.process(input);

		Assert.assertEquals(resultProcessingInstruction2, result);
	}

	@Test
	public void shouldReturnInputIfProcessingInstructionsIsEmptyWhenProcessing() {
		ChainedProcessingInstruction processingInstruction = new ChainedProcessingInstruction(Collections.emptyList());
		String input = "abc";

		CharSequence result = processingInstruction.process(input);

		Assert.assertEquals(input, result);
	}

}
