package com.gitlab.spacetrucker.regexprocessing;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

public class ChainedProcessingInstruction implements ProcessingInstruction {

	private final List<ProcessingInstruction> processingInstructions;

	public ChainedProcessingInstruction(List<? extends ProcessingInstruction> processingInstructions) {
		super();
		this.processingInstructions = Collections.unmodifiableList(new ArrayList<>(processingInstructions));
	}

	@Override
	public CharSequence process(CharSequence input) {
		CharSequence current = input;
		for (ProcessingInstruction processingInstruction : processingInstructions) {
			current = processingInstruction.process(current);
		}
		return current;
	}

	public List<ProcessingInstruction> getProcessingInstructions() {
		return processingInstructions;
	}

}
