package com.gitlab.spacetrucker.regexprocessing;

import java.io.InputStream;
import java.io.PrintStream;
import java.nio.charset.Charset;

import com.beust.jcommander.JCommander;

public class RegexProcessing implements Runnable {

	private final RegexProcessingOptions options;
	private final InputStream inputStream;
	private final PrintStream outputPrintStream;
	private final ProcessingInstructionParser processingInstructionParser;

	public RegexProcessing(RegexProcessingOptions options, InputStream inputStream, PrintStream outputPrintStream,
			ProcessingInstructionParser processingInstructionParser) {
		super();
		this.options = options;
		this.inputStream = inputStream;
		this.outputPrintStream = outputPrintStream;
		this.processingInstructionParser = processingInstructionParser;
	}

	@Override
	public void run() {
		ProcessingInstruction processingInstruction = processingInstructionParser
				.parseProcessingInstructions(options.getCommandlineProcessingInstructions());

		CharSequence input = Input.readInput(inputStream, Charset.defaultCharset());

		CharSequence output = processingInstruction.process(input);

		writeOutput(output, outputPrintStream);
	}

	private void writeOutput(CharSequence output, PrintStream printStream) {
		printStream.append(output);
		printStream.flush();
	}

	public static RegexProcessing createDefaultFromOptions(RegexProcessingOptions options) {
		return new RegexProcessing(options, System.in, System.out, new ProcessingInstructionParserImpl());
	}

	public static void main(String[] args) {
		RegexProcessingOptions options = new RegexProcessingOptions();
		JCommander jCommander = new JCommander(options);
		jCommander.parse(args);

		if (options.isHelp()) {
			jCommander.usage();
			System.exit(0);
		}

		RegexProcessing instance = createDefaultFromOptions(options);
		try {
			instance.run();
			System.exit(0);
		} catch (RuntimeException e) {
			e.printStackTrace(System.err);
			System.exit(1);
		}
	}
}
