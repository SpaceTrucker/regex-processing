package com.gitlab.spacetrucker.regexprocessing;

import java.util.regex.Matcher;
import java.util.regex.Pattern;
import java.util.regex.PatternSyntaxException;

public class PatternProcessingInstruction implements ProcessingInstruction {

	private final Pattern pattern;
	private final String replacement;

	public PatternProcessingInstruction(Pattern pattern, String replacement) {
		super();
		this.pattern = pattern;
		this.replacement = replacement;
	}

	@Override
	public String process(CharSequence input) {
		Matcher matcher = pattern.matcher(input);
		return matcher.replaceAll(replacement);
	}

	public Pattern getPattern() {
		return pattern;
	}

	public String getReplacement() {
		return replacement;
	}

	public static PatternProcessingInstruction createProcessingInstruction(String regex, String replacement) {
		Pattern compiledPattern;
		try {
			compiledPattern = Pattern.compile(regex);
		} catch (PatternSyntaxException e) {
			throw new IllegalArgumentException(
					"Invalid regex " + regex + " in processing instructions: " + e.getMessage(), e);
		}
		return new PatternProcessingInstruction(compiledPattern, replacement);
	}
}
