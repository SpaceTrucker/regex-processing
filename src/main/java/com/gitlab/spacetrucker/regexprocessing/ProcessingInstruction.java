package com.gitlab.spacetrucker.regexprocessing;

public interface ProcessingInstruction {

	CharSequence process(CharSequence input);

}
