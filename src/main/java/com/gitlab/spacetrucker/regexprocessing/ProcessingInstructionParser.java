package com.gitlab.spacetrucker.regexprocessing;

import java.util.List;

public interface ProcessingInstructionParser {

	ProcessingInstruction parseProcessingInstructions(List<String> processingInstructions);

}
