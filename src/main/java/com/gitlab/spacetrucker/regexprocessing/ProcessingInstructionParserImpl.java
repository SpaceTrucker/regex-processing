package com.gitlab.spacetrucker.regexprocessing;

import java.util.ArrayList;
import java.util.List;
import java.util.ListIterator;

public class ProcessingInstructionParserImpl implements ProcessingInstructionParser {

	@Override
	public ChainedProcessingInstruction parseProcessingInstructions(List<String> processingInstructions) {
		List<PatternProcessingInstruction> parsedProcessingInstructions = new ArrayList<>();
		ListIterator<String> iter = processingInstructions.listIterator();
		while (iter.hasNext()) {
			String regex = iter.next();
			int oneBasedRegexIndex = iter.previousIndex() + 1;
			if (!iter.hasNext()) {
				throw new IllegalArgumentException("Missing replacement for last regex.");
			} else {
				String replacement = iter.next();
				PatternProcessingInstruction processingInstruction;
				try {
					processingInstruction = PatternProcessingInstruction.createProcessingInstruction(regex,
							replacement);
				} catch (IllegalArgumentException e) {
					throw new IllegalArgumentException("Processing instruction starting at index " + oneBasedRegexIndex
							+ " is invalid: " + e.getMessage(), e);
				}
				parsedProcessingInstructions.add(processingInstruction);
			}
		}
		return new ChainedProcessingInstruction(parsedProcessingInstructions);
	}
}