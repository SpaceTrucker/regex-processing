package com.gitlab.spacetrucker.regexprocessing;

import java.util.ArrayList;
import java.util.List;

import com.beust.jcommander.Parameter;

public class RegexProcessingOptions {


	@Parameter(names = { "-i",
			"--instructions" }, description = "list of processing instructions consisting of a regular expression and a replacement", variableArity = true)
	private List<String> commandlineProcessingInstructions = new ArrayList<>();

	@Parameter(names = { "-h", "--help" }, help = true)
	private boolean help;

	public RegexProcessingOptions() {
	}

	public RegexProcessingOptions(List<String> commandlineProcessingInstructions, boolean help) {
		super();
		this.commandlineProcessingInstructions = commandlineProcessingInstructions;
		this.help = help;
	}

	public List<String> getCommandlineProcessingInstructions() {
		return commandlineProcessingInstructions;
	}

	public boolean isHelp() {
		return help;
	}

}
