package com.gitlab.spacetrucker.regexprocessing;

import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.nio.charset.Charset;

public final class Input {

	private static final int CHAR_BUFFER_SIZE = 8192;
	private static final int END_OF_STREAM = -1;

	private static final IllegalStateException NOT_ENOUGH_MEMORY_ILLEGAL_STATE;

	static {
		NOT_ENOUGH_MEMORY_ILLEGAL_STATE = new IllegalStateException("Not enough memory to consume input.");
		NOT_ENOUGH_MEMORY_ILLEGAL_STATE.setStackTrace(new StackTraceElement[0]);
	}

	private Input() {
	}

	public static CharSequence readInput(InputStream input, Charset charset) {
		try {
			InputStreamReader reader = new InputStreamReader(input, charset);
			StringBuilder sb = new StringBuilder();
			char[] buffer = new char[CHAR_BUFFER_SIZE];
			int readChars;
			do {
				try {
					readChars = reader.read(buffer);
				} catch (IOException e) {
					throw new IllegalStateException("Error while reading input.", e);
				}
				if (readChars > 0) {
					sb.append(buffer, 0, readChars);
				}
			} while (readChars != END_OF_STREAM);
			return sb;
		} catch (OutOfMemoryError e) {
			throw NOT_ENOUGH_MEMORY_ILLEGAL_STATE;
		}
	}

}
