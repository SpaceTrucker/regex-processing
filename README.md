`regex-processing` is a small command line utility for processing input using multiple regular expression replacements.

Input is read from standard input and the specified regular expression replacements are applied to it. After processing the result is written to standard output. 

    Usage: <main class> [options]
      Options:
        -h, --help
    
          Default: false
        -i, --instructions
          list of processing instructions consisting of a regular expression and a 
          replacement 
          Default: []

